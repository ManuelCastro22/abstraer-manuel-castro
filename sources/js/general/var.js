/* GENERAL */

const win = window,
    doc = document

const changeFunctions = ( oldName, newName ) => {
    [Window, Document, Element].map( selector => {
        selector.prototype[`${oldName}`] = selector.prototype[`${newName}`]
    } )
}

const addEv = changeFunctions('addEv', 'addEventListener'),
    que = changeFunctions('que', 'querySelector'),
    queAll = changeFunctions('queAll', 'querySelectorAll'),
    gId = changeFunctions('gId', 'getElementById')

export {win, doc, addEv, que, queAll, gId}