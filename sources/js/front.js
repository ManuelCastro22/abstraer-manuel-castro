/* IMPORT GENERAL */
import {
	win,
	doc,
	addEv,
	que,
	queAll,
	gId
} from "./general/var.js"


/* IMPORTS */
import { openLoader } from './components/loader.js'
import { carousel } from './components/carousel'
import { modal } from './components/modal.js'
import { scrollBar } from "./components/scrollBar.js"
import { choicesSelect } from "./components/choicesSelect.js"
import { datePicker } from "./components/datePicker.js"
import { validateForm } from "./components/validateForm.js"
// import { customPropertiesPolyfill } from './partials/customPropertiesPolyfill.js'
// import 'regenerator-runtime/runtime'


/* ON LOAD */
win.onload = () => doc.que('.loader').classList.add('hide')


/* LOAD COMPLETE */
doc.addEv('DOMContentLoaded', e => {


	// OPEN LOADER
	openLoader()


	// CAROUSEL
	carousel('.carousel')


	// MODAL
	modal()


	// SCROLL BAR
	scrollBar()


	// CHOICES SELECT
	choicesSelect()


	// DATEPICKER
	datePicker()


	// VALIDATE FORM
	validateForm()






	// CUSTOM PROPERTIES POLYFILL
	// customPropertiesPolyfill()


	// ALERTS
	// alerts()

	// MENU
	// menu()

	// MODAL
	// modal()

	// CHOICES ???

	// SET PROPERY
	// d.documentElement.style.setProperty('--bg-example', 'purple');


})