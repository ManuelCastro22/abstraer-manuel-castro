// IMPORTS
import {
	win,
	doc,
	addEv,
	que,
	queAll,
    gId } from '../general/var.js'
// Import Overlay scrollbar
import OverlayScrollbars from 'overlayscrollbars'
import 'overlayscrollbars/css/OverlayScrollbars.css'



export const scrollBar = () => {

	const modalContent = doc.que('.modal__content')


	createScrollBar(modalContent)


	// CREATE SCROLLBAR
	function createScrollBar(element) {
		OverlayScrollbars(element, {
			scrollbars: {
				autoHide: "move",
				autoHideDelay: 800
			},
			nativeScrollbarsOverlaid: {
				initialize: false
			}
		})
	}

}