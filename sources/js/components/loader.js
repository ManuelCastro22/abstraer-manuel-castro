// IMPORTS
import {win, doc, addEv, que, queAll, gId} from "../general/var.js"


// OPEN LOADER
export const openLoader = () => {
    const oLoader = doc.queAll(`.oLoader, a[href*="/"], a[href*="."]`)

    oLoader.length > 0 && [...oLoader].map( el => {
        el.addEventListener('click', () => {
            doc.que('.loader').classList.remove('hide')
        })
    } )
}