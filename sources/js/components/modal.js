import {win, doc, addEv, que, queAll, gId} from "../general/var"


// MODAL
export const modal = () => {
    const modal = doc.queAll('.modal'),
        closeModal = e => {

            let el = e.target,
                btnBookNow = doc.que('.btn[data-modal="bookNow"].active')

            if( e.target === e.currentTarget ){
                if( !el.classList.contains('modal') ){
                    e.preventDefault()
                }
                Array.from(doc.queAll('.modal')).map( el => {

                    el.classList.add('hide')
                    btnBookNow && btnBookNow.classList.remove('active')
                    doc.body.classList.remove('noScroll')

                } )
            }

        },
        openModal = e => {
            let dataEl = e.currentTarget.dataset.modal
            if( !e.currentTarget.classList.contains('modal') ){
                e.preventDefault()
                doc.que(`.modal[data-modal="${dataEl}"]`).classList.remove('hide')
            }

            dataEl === 'bookNow' && doc.que('.btn[data-modal="bookNow"]').classList.add('active')

            doc.body.classList.add('noScroll')
        }

    if( modal.length > 0 ){
        Array.from(doc.queAll('.modal, .modal__close, .modal__btn--close')).map( el => {
            el.addEventListener('click', e => {
                closeModal(e)
            })
        } )

        Array.from(doc.queAll('[data-modal]:not(.modal)')).map( el => {
            el.addEventListener('click', e => {
                openModal(e)
            })
        } )
    }
}