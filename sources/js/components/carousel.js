/* IMPORT GENERAL */
import { win,
	doc,
	addEv,
	que,
	queAll,
    gId } from "../general/var.js"
// IMPORT SWIPER
import Swiper, { Navigation, Pagination, Autoplay } from 'swiper'
import 'swiper/swiper-bundle.css'



// Configure Swiper
Swiper.use([ Navigation, Pagination, Autoplay ])


export const carousel = element => {

    return createCarousel(element)

}



// function to create the swiper carousel
function createCarousel(element){

    const carousel = new Swiper(element, {
        speed: 1000,
        autoplay: {
            delay: 8000
        },
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.historyBox__line',
            type: 'progressbar'
        }
    })

}