// IMPORTS
import {
	win,
	doc,
	addEv,
	que,
	queAll,
    gId } from '../general/var.js'
// Import choices
import Pikaday from 'pikaday'
import "pikaday/css/pikaday.css"



export const datePicker = () => {

	const datePicker = doc.queAll('.form__input--date')

	datePicker.length > 0 && Array.from(datePicker).map( pickerItem => {
		const picker = new Pikaday({ field: pickerItem });
	} )

}