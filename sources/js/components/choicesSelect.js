// IMPORTS
import {
	win,
	doc,
	addEv,
	que,
	queAll,
    gId } from '../general/var.js'
// Import choices
import Choices from 'choices.js'
import "choices.js/public/assets/styles/choices.min.css"



export const choicesSelect = () => {

	const selects = doc.queAll('.select')

	selects.length > 0 && Array.from(selects).map( selectEl => {

		const selectSimple = new Choices( selectEl, {
			classNames: {
				containerOuter: 'choices select'
			}
		} )

	})

}